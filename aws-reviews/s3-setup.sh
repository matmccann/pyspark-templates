aws s3 mb $1 --region us-west-2
aws s3 cp $2 "${1}/${2}"
aws s3 cp $3 "${1}/${3}"
