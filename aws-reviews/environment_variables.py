import os


def environment_variables():
    s3_bucket_path = os.environ['s3_bucket_path']
    key_name = os.environ['key_name']
    print(s3_bucket_path)
    print(key_name)


if __name__ == "__main__":
    environment_variables()
