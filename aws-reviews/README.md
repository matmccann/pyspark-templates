# AWS-REVIEWS
AWS EMR job on aws-reviews using s3 buckets and pyspark with aws emr.


## Describe here your project
This is simple setup that pulls from a public s3 bucket, processes with EMR, and outputs to your s3 bucket.

# How to run  
1. ensure that your awscli is up and running and file permissions are set.

```bash
$ aws --version
aws-cli/1.16.266 Python/3.7.4 Windows/10 botocore/1.13.2
$ ls -al
-rwxr-xr-x 1 matmc 197610   81 Nov  9 14:39 emr_bootstrap.sh
-rw-r--r-- 1 matmc 197610  474 Nov  9 14:49 job-submit-cli.sh
-rw-r--r-- 1 matmc 197610 1896 Nov  9 14:39 pyspark_job.py
-rw-r--r-- 1 matmc 197610    0 Nov  9 14:39 README.md
```

2. setup s3 buckets by passing name-of-bucket to create
- e.g. 
    $1 = bucket_name = s3://aa070d3exx
    $2 = bootstrap = emr_bootstrap.sh
    $3 = job_file = pyspark_job.py

```bash
$ ./s3-setup.sh s3://aa070d3e-ee82-4ec6-9fdy/aws-reviews emr_bootstrap.sh pyspark_job.py
```

3. Use AWS Cli to submit a job to EMR
- for simplicity, move your aws.pem (mine is called emr.pem) into the same folder as the cli script.
- run the cli-script where $1 is S3 BUCKET PATH and $2 is YOUR PEM.
- e.g.
    $1 = bucket_name = s3://aa070d3e-testeroo
    $2 = key_name = emr.pem
    $3 = bootstrap = emr_bootstrap.sh
    $4 = job_file = pyspark_job.py

```bash
$ ./job-submit-cli.sh s3://aa070d3e-ee82-4ec6-9fdy/aws-reviews emr emr_bootstrap.sh pyspark_job.py
{
    "ClusterId": "j-2JQRX9G1V5PIJ"
}
```

# Expected outcome
1. go to aws emr -> clusters:
    name :Spark cluster with step
    status: Terminated      All steps completed
    elapsed time: 8 minutes

- take note of "ID" from the job-submit-cli.sh. This will be the folder where your logs go in s3.

2. go to s3-> whatever-you-put-for-bucket_name
bucket_name/logs/ID: stuff should be here
bucket_name/output: you have an "_SUCCESS" file with ~200 .parquete files (1.5kB each)
