# AWS-REVIEWS
AWS EMR job on aws-reviews using s3 buckets and pyspark with aws emr.


## Describe here your project
This is simple setup that pulls from a public s3 bucket, processes with EMR, and outputs to your s3 bucket.

# How to run  
1. ensure that your awscli is up and running and file permissions are set.
2. setup s3 buckets by passing variables where
    $1 = bucket_name = s3://aa070d3e-template
    $2 = bootstrap_file = emr_bootstrap.sh
    $3 = emr_job = main_task2.py
    $4 = file_name = emr-input.txt

```bash
$ ./s3-setup.sh s3://aa070d3e-template emr_bootstrap.sh main_task2.py emr-input.txt
```

3. Use AWS Cli to submit a job to EMR
- for simplicity, move your aws.pem (mine is called emr.pem) into the same folder as the cli script.
- use aws-cli script to submit spark job passing variables where
    $1 = bucket_name = s3://aa070d3e-testeroo
    $2 = key_name = emr.pem
    $3 = bootstrap_file = emr_bootstrap.sh
    $4 = emr_job = main_task2.py
    $5 = file_name = emr-input.txt

```bash
$ ./job-submit-cli.sh s3://aa070d3e-template emr emr_bootstrap.sh main_task2.py emr-input.txt
{
    "ClusterId": "j-2JQRX9G1V5PIJ"
}
```

# Expected outcome
1. go to aws emr -> clusters:
    name :Spark cluster with step
    status: Terminated      All steps completed
    elapsed time: 6 minutes

- take note of "ID" from the job-submit-cli.sh. This will be the folder where your logs go in s3.

2. go to s3-> whatever-you-put-for-bucket_name
bucket_name/logs/ID: stuff should be here
bucket_name/output: this director will get created if the job completes successfully.
- note: failed jobs will terminate with errors, and you can view errors in aws console or in your /logs bucket
