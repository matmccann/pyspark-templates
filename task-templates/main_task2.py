from __future__ import print_function

import os
import sys

from pyspark import SparkContext


def create_spark_session():
    """Create spark session.
    Returns:
            spark (SparkSession) - spark session connected to AWS EMR
                cluster
    """
    spark = SparkSession \
        .builder \
        .config("spark.jars.packages",
                "org.apache.hadoop:hadoop-aws:2.7.0") \
        .getOrCreate()
    return spark


def process_data(spark, input_path, output_path):
    """Process the input data and write to S3.
        Arguments:
            spark (SparkSession) - spark session connected to AWS EMR
                cluster
            input_path (str) - AWS S3 bucket path for source data
            output_path (str) - AWS S3 bucket for writing processed data
    """

    lines = spark.textFile(input_file, 1)
    counts = lines.flatMap(lambda x: x.split(' ')).map(lambda x: (x, 1)).reduceByKey(add)
    lines.parallelize(counts).saveAsTextFile(output_path)


if __name__ == "__main__":

    spark = create_spark_session()
    input_file = sys.argv[0] + '/' + sys.argv[1]
    output_path = sys.argv[0] + '/output'
    process_data(spark, input_file, output_path)
