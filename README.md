# pyspark-template
This is a pyspark project template for submitting jobs to AWS-EMR with the aws-cli.
<br />
S3 buckets act as input/output to jobs where EMR services are automatically provisioned and terminated through the cli.
<br />
## aws-reviews
```bash
functionality: word count instance.
input: parquetes of aws-reviews from public s3 bucket.
output: store logs and result your new public bucket that is automatically provisioned.
```
## task-templates
- generalized templates for submitting a single job to emr
- ability to create job with the following inputs:

```bash
    1. name for a new s3 bucket.
    2. name of your pem key located in same directory as shell script being run.
    3. name of the bootstrap file for emr environment.
    4. name of script that executes a step, or distributed process, on the input data .
    5. name of the input data (functions exist for handling text, csv, and parquette)
```
